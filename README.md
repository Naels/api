# Art Gallery Web Service

## Description

The Art Gallery Web Service is a crucial component of the Art Gallery Online project. This service acts as the backend system that handles various operations, including retrieving artwork data, managing artist profiles. It is designed to seamlessly connect with the Art Gallery Database and provide the necessary APIs for the frontend application.

Key Features

Artwork Retrieval: The web service retrieves artwork data from the Art Gallery Database and provides it to the frontend application in a structured format. This includes information such as artwork title, description, design...

Artist Profile Retrieval: The service retrieves artist data from the Art Gallery Database and provides it to the frontend application in a structured format. You will get all the informations about the artist and also all the artwork he has done


### Installation and Setup

1. Clone the repository for the Art Gallery Web Service
2. Install the required dependencies => `npm i`
3. Configure the database connection settings in the service configuration file.
4. Create the database using the Art Gallery file containing SQL commands
5. Run the service locally or deploy it to a web server. `npm start` 

### Technologies Used

1. Programming Language: Java
2. Framework: SpringBoot
3. Database: MySQL
