package com.nsa.springboot.service;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Objects;

@Service
public class ImageUploadService {

    private static final String UPLOAD_DIR = "";

    public String uploadImage(MultipartFile file) throws IOException {
        // Generate a unique filename
        String filename = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        String uniqueFilename = System.currentTimeMillis() + "-" + filename;

        // Set the target file path
        Path targetFilePath = Path.of(UPLOAD_DIR, uniqueFilename);

        // Copy the uploaded file to the target location
        Files.copy(file.getInputStream(), targetFilePath, StandardCopyOption.REPLACE_EXISTING);

        return uniqueFilename;
    }

    public boolean deleteImage(String filename) throws IOException {
        // Set the file path to delete
        Path targetFilePath = Path.of(UPLOAD_DIR, filename);

        // Delete the file if it exists
        if (Files.exists(targetFilePath)) {
            Files.delete(targetFilePath);
            return true;
        }

        return false;
    }
}