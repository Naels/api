package com.nsa.springboot.service;

import com.nsa.springboot.entity.Artwork;
import com.nsa.springboot.repository.ArtworkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ArtworkService {

    private final ArtworkRepository artworkRepository;

    @Autowired
    public ArtworkService(ArtworkRepository artworkRepository) {
        this.artworkRepository = artworkRepository;
    }

    public List<Artwork> getAllArtworks() {
        return artworkRepository.findAll();
    }

    public Optional<Artwork> getArtworkById(Long id) {
        return artworkRepository.findById(id);
    }

    public List<Artwork> getArtworkByArtistId(Long id){
        return artworkRepository.findByArtistId(id);
    }
    public List<Artwork> getArtworkByArtsit(String name) {
        return artworkRepository.findByArtistContainingIgnoreCase(name);
    }

    public Artwork createArtwork(Artwork artwork) {
        return artworkRepository.save(artwork);
    }

    public Artwork updateArtwork(Long id, Artwork artwork) {
        Optional<Artwork> existingArtworkOptional = artworkRepository.findById(id);
        if (existingArtworkOptional.isPresent()) {
            Artwork existingArtwork = existingArtworkOptional.get();
            existingArtwork.setTitle(artwork.getTitle());
            existingArtwork.setDescription(artwork.getDescription());
            existingArtwork.setArtist(artwork.getArtist());
            existingArtwork.setImageUrl(artwork.getImageUrl());
            return artworkRepository.save(existingArtwork);
        }
        return null;
    }

    public void deleteArtwork(Long id) {
        artworkRepository.deleteById(id);
    }
}
