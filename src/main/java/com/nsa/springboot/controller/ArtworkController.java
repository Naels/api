package com.nsa.springboot.controller;


import com.nsa.springboot.entity.Artwork;
import com.nsa.springboot.service.ArtworkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/artworks")
public class ArtworkController {

    private final ArtworkService artworkService;

    @Autowired
    public ArtworkController(ArtworkService artworkService) {
        this.artworkService = artworkService;
    }

    @GetMapping
    public ResponseEntity<List<Artwork>> getAllArtworks() {
        List<Artwork> artworks = artworkService.getAllArtworks();
        return ResponseEntity.ok(artworks);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Artwork> getArtworkById(@PathVariable Long id) {
        Optional<Artwork> artworkOptional = artworkService.getArtworkById(id);
        return artworkOptional.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/artist/id/{id}")
    public ResponseEntity<List<Artwork>> getArtworkByArtistId(@PathVariable Long id) {
        List<Artwork> artworkList = artworkService.getArtworkByArtistId(id);
        return ResponseEntity.ok(artworkList);
    }

    @GetMapping("/artist/{artist}")
    public ResponseEntity<List<Artwork>> getArtworkByArtist(@PathVariable String artist) {
        List<Artwork> artworkList = artworkService.getArtworkByArtsit(artist);
        return ResponseEntity.ok(artworkList);
    }

    @GetMapping("/random")
    public List<Artwork> getRandomArtwork() {
        List<Artwork> allArtwork = artworkService.getAllArtworks();
        int limit = Math.min(allArtwork.size(), 5);
        return new Random().ints(0, allArtwork.size())
                .distinct()
                .limit(limit)
                .mapToObj(allArtwork::get)
                .collect(Collectors.toList());
    }

    @PostMapping
    public ResponseEntity<Artwork> createArtwork(@RequestBody Artwork artwork) {
        Artwork createdArtwork = artworkService.createArtwork(artwork);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdArtwork);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Artwork> updateArtwork(@PathVariable Long id, @RequestBody Artwork artwork) {
        Artwork updatedArtwork = artworkService.updateArtwork(id, artwork);
        if (updatedArtwork != null) {
            return ResponseEntity.ok(updatedArtwork);
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteArtwork(@PathVariable Long id) {
        artworkService.deleteArtwork(id);
        return ResponseEntity.noContent().build();
    }
}