package com.nsa.springboot.repository;

import com.nsa.springboot.entity.Artwork;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ArtworkRepository extends JpaRepository<Artwork, Long> {

    @Override
    Optional<Artwork> findById(Long id);
    List<Artwork> findByArtistId(Long id);

    List<Artwork> findByTitleContainingIgnoreCase(String title);

    List<Artwork> findByArtistContainingIgnoreCase(String artist);

}
