package com.nsa.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.nsa.springboot", "com.nsa.springboot.config"})
public class ArtVistaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArtVistaApplication.class, args);
	}

}
