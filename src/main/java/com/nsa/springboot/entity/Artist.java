package com.nsa.springboot.entity;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Entity
@Table(name = "artist")
public class Artist {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String nationality;

    @Column(nullable = true)
    private String artistStyle;

    @OneToMany(mappedBy = "artist", cascade = CascadeType.ALL)
    private List<Artwork> artworks;

    // Constructors

    public Artist() {
    }

    public Artist(String name, String nationality, String artistStyle) {
        this.name = name;
        this.nationality = nationality;
        this.artistStyle = artistStyle;
    }

    // Getters and Setters

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public List<String> getArtistStyleList() {
        if (artistStyle == null || artistStyle.isEmpty()) {
            return Collections.emptyList();
        }
        return Arrays.asList(artistStyle.split(","));
    }

    public void setArtistStyleList(List<String> artistStyleList) {
        if (artistStyleList == null || artistStyleList.isEmpty()) {
            this.artistStyle = null;
        } else {
            this.artistStyle = String.join(",", artistStyleList);
        }
    }

    public List<Artwork> getArtworks() {
        return artworks;
    }

    public void setArtworks(List<Artwork> artworks) {
        this.artworks = artworks;
    }

}