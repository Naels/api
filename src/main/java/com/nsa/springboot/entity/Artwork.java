package com.nsa.springboot.entity;

import javax.persistence.*;

@Entity
@Table(name = "artwork")
public class Artwork {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private String artist;

    @Column(nullable = false)
    private String imageUrl;

    @Column(nullable=false)
    private Long artistId;

    // Constructors

    public Artwork() {
    }

    public Artwork(String title, String description, String artist, String imageUrl, Long artistId) {
        this.title = title;
        this.description = description;
        this.artist = artist;
        this.imageUrl = imageUrl;
        this.artistId = artistId;
    }

    // Getters and Setters

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Long getArtistId(){
        return artistId;
    }

    public void setArtistId(Long artistId){
        this.artistId = artistId;
    }

    // Other methods

    @Override
    public String toString() {
        return "Artwork{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", artist='" + artist + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }
}
